package app;

import models.Channel;
import models.Controller;
import models.Program;
import models.TV;

public class Application {

    public static void main(String[] args) {
        TV tv = new TV("Xiaomi");
        Channel channel = new Channel("BBC");
        Program p1 = new Program("Top Gear");
        Program p2 = new Program("Bob Constructor");
        Program p3 = new Program("Doctors");

        Controller controller = new Controller(tv);

        tv.addChannel(channel);
        channel.addProgram(p1);
        channel.addProgram(p2);
        channel.addProgram(p3);

        controller.on(1);
    }
}










