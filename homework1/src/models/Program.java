package models;

/**
 * 02.09.2021
 * homework1
 *
 * @author Nikolay Ponomarev
 * @version 1.0
 */
public class Program {
    private final String programName;

    public Program(String programName) {
        this.programName = programName;
    }

    public String getProgramName() {
        return programName;
    }
}





