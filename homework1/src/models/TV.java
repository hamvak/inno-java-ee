package models;

/**
 * 02.09.2021
 * homework1
 *
 * @author Nikolay Ponomarev
 * @version 1.0
 */
public class TV {
    private final String modelName;
    private final Channel[] channels;
    private int channelCount = 1;

    public TV(String modelName) {
        this.modelName = modelName;
        this.channels = new Channel[3];
    }

    public Channel getChannels(int number) {
        return channels[number];
    }

    /**
     * Процедура добавления <code>channel</code> в массив <code>channels[]</code>
     * @param channel  добавляемая программа
     */
    public void addChannel(Channel channel) {
        this.channels[channelCount] = channel;
        channelCount++;
    }
}







