package models;

import java.util.Random;

/**
 * 02.09.2021
 * homework1
 *
 * @author Nikolay Ponomarev
 * @version 1.0
 */
public class Controller {
    private final TV tv;

    Random random = new Random();

    public Controller(TV tv) {
        this.tv = tv;
    }

    /**
     * Процедура выбора случайной <code>program</code> из массива <code>programs[]</code>
     * с последующим выводом <code>channelName</code> и <code>programName</code>
     * @param number номер нужного канала
     */
    public void on(int number) {
        int randomProgram = random.nextInt(3);
        String channel = tv.getChannels(number).getChannelName();
        String program = tv.getChannels(number).getPrograms(randomProgram).getProgramName();

        System.out.println("You are choose channel - " + channel + " // " + "Now playing - " + program);
    }
}






