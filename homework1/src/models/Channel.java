package models;

/**
 * 02.09.2021
 * homework1
 *
 * @author Nikolay Ponomarev
 * @version 1.0
 */
public class Channel {
    private final String channelName;
    private final Program[] programs;
    private int programCount;

    public Channel(String channelName) {
        this.channelName = channelName;
        this.programs = new Program[3];
    }

    public String getChannelName() {
        return channelName;
    }

    public Program getPrograms(int number) {
        return programs[number];
    }

    /**
     * Процедура добавления <code>program</code> в массив <code>programs[]</code>
     * @param program добавляемая программа
     */
    public void addProgram(Program program) {
        this.programs[programCount] = program;
        programCount++;
    }
}






